$user = 'railstest'
$group = 'railstest'
$user_home  = '/tmp/railstest'

user { $user: 
  ensure => present,
  home => $user_home, 
  password => '!!',
  managehome => true,
}

group { $group: ensure => present, }

rails::instance { 'smoketest-rails':
  user => $user,
  user_home => $user_home,
  ruby_version => '2.0.0',
  ruby_build => 'p353',
  rbenv_ref => 'master',
  rbenv_ruby_build_ref => 'master',
  rails_version => '4.1.0',
}
