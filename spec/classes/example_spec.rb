require 'spec_helper'

describe 'rails' do
  context 'supported operating systems' do
    ['Debian', 'RedHat'].each do |osfamily|
      describe "rails class without any parameters on #{osfamily}" do
        let(:params) {{ }}
        let(:facts) {{
          :osfamily => osfamily,
        }}

        it { should compile.with_all_deps }

        it { should contain_class('rails::params') }
        it { should contain_class('rails::install').that_comes_before('rails::config') }
        it { should contain_class('rails::config') }
        it { should contain_class('rails::service').that_subscribes_to('rails::config') }

        it { should contain_service('rails') }
        it { should contain_package('rails').with_ensure('present') }
      end
    end
  end

  context 'unsupported operating system' do
    describe 'rails class without any parameters on Solaris/Nexenta' do
      let(:facts) {{
        :osfamily        => 'Solaris',
        :operatingsystem => 'Nexenta',
      }}

      it { expect { should contain_package('rails') }.to raise_error(Puppet::Error, /Nexenta not supported/) }
    end
  end
end
