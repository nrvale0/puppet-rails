class rails::params() {
  case $::osfamily {
    'redhat': {}
    default: {fail("OS family ${::osfamily} not supported by this class!")}
  }
}
