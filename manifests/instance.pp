define rails::instance(
  $ruby_version,
  $ruby_build,
  $rails_version,
  $user_home,
  $user = $name,
  $rbenv_ref,
  $rbenv_ruby_build_ref,
  $gems = [],
) {

  include ::rails
  include ::rails::params

  validate_string($user, $ruby_version, $ruby_build, $rails_version)
  validate_string($rbenv_ref, $rbenv_ruby_build_ref)
  validate_absolute_path($user_home)

  rbenv::install { $user: 
    user => $user, 
    group => $group, 
    user_home => $user_home,
    rbenv_ref => $rbenv_ref,
    rbenv_ruby_build_ref => $rbenv_ruby_build_ref,
    ruby_version => "${ruby_version}-${ruby_build}",
    global_ruby_version => "${ruby_version}-${ruby_build}",
    gems => flatten([ "rails:${rails_version}", $gems ])
  }
}
